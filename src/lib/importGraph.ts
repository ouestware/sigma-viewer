import Graph from "graphology";
import { FileWithPath } from "react-dropzone";

// @ts-ignore
import gexf from "graphology-gexf/browser";

type Loader = (text: string) => Graph;

const Loaders: { [extension: string]: Loader } = {
  gexf: (text) => gexf.parse(Graph, text),
};

export async function loadLocalGraphFile(file: FileWithPath): Promise<Graph> {
  const ext = (file.path!.split(".").pop() || "").toLowerCase();

  if (!Loaders[ext]) throw new Error(`Graph file extension ".${ext}" not recognized.`);

  const text = await file.text();
  return Loaders[ext](text);
}

export async function loadHTTPGraphFile(path: string): Promise<Graph> {
  const ext = (path.split(".").pop() || "").toLowerCase();

  if (!Loaders[ext]) throw new Error(`Graph file extension ".${ext}" not recognized.`);

  const text = await fetch(path).then((res) => res.text());
  return Loaders[ext](text);
}
