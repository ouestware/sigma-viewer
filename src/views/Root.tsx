import React, { FC, useState } from "react";

import Loader from "./Loader";
import Graph from "graphology";
import Sigma from "./Sigma";

const Root: FC = () => {
  const [graph, setGraph] = useState<Graph | null>(null);

  return graph ? <Sigma graph={graph} /> : <Loader onGraphLoaded={setGraph} />;
};

export default Root;
