import React, { Component, createRef, RefObject } from "react";
import { toPairs, omit } from "lodash";
import { WebGLRenderer } from "sigma";
import Graph from "graphology";

import "../styles/sigma.css";

interface PropsType {
  graph: Graph;
}

interface StateType {
  showSidePanel: boolean;
}

const DURATION = 200;

/**
 * Tiny helper to transform "camelCaseStrings" to "Printable strings":
 */
function prettify(str: string): string {
  let res = str.replace(/([A-Z])/g, " $1");
  res = res[0].toUpperCase() + res.slice(1);
  return res;
}

class Sigma extends Component<PropsType, StateType> {
  domRoot: RefObject<HTMLDivElement> = createRef<HTMLDivElement>();
  sigma?: WebGLRenderer;
  state: StateType = { showSidePanel: false };

  componentDidMount() {
    this.initSigma();
  }
  componentWillUnmount() {
    this.killSigma();
  }
  componentDidUpdate(prevProps: PropsType) {
    if (prevProps.graph !== this.props.graph) {
      if (this.props.graph) {
        this.initSigma();
      } else {
        this.killSigma();
      }
    }
  }

  initSigma() {
    if (!this.domRoot.current) return;
    if (this.sigma) this.killSigma();

    this.sigma = new WebGLRenderer(this.props.graph, this.domRoot.current, {
      labelFont: "Lato, sans-serif",
    });
  }
  killSigma() {
    if (this.sigma) {
      this.sigma.kill();
      this.sigma = undefined;
    }
  }
  zoom(ratio?: number) {
    if (!this.sigma) return;

    if (!ratio) {
      this.sigma.getCamera().animatedReset({ duration: DURATION });
    } else if (ratio > 0) {
      this.sigma.getCamera().animatedZoom({ duration: DURATION });
    } else if (ratio < 0) {
      this.sigma.getCamera().animatedUnzoom({ duration: DURATION });
    }
  }

  toggleSidePanel() {
    this.setState(
      {
        ...this.state,
        showSidePanel: !this.state.showSidePanel,
      },
      () => this.sigma!.render()
    );
  }

  render() {
    const { graph } = this.props;
    const { showSidePanel } = this.state;
    const graphAttributes = graph.getAttributes();

    return (
      <main className="sigma">
        {showSidePanel ? (
          <div className="side-panel">
            <p>
              <button className="ico" onClick={() => this.toggleSidePanel()} title="Hide side panel">
                <i className="fas fa-angle-double-left" />
              </button>
            </p>
            {graphAttributes.title && <h1>{graphAttributes.title}</h1>}
            {toPairs(omit(graphAttributes, ["title"]))
              .filter(([key, value]) => key && value)
              .map(([key, value]) => (
                <p>
                  <strong>{prettify(key)}:</strong> {value}
                </p>
              ))}{" "}
            <br />
            <p>
              <strong>Graph order:</strong> {graph.order} node{graph.order > 1 ? "s" : ""}
            </p>
            <p>
              <strong>Graph size:</strong> {graph.size} edge{graph.size > 1 ? "s" : ""}
            </p>
          </div>
        ) : (
          <div className="top-left-button">
            <button className="ico" onClick={() => this.toggleSidePanel()} title="Show side panel">
              <i className="fas fa-angle-double-right" />
            </button>
          </div>
        )}

        <div className="graph-display">
          <div className="sigma-container" ref={this.domRoot} />

          <div className="controls">
            <button className="ico" onClick={() => this.zoom(1)} title="Zoom">
              <i className="fas fa-search-plus" />
            </button>

            <button className="ico" onClick={() => this.zoom(-1)} title="Unzoom">
              <i className="fas fa-search-minus" />
            </button>

            <button className="ico" onClick={() => this.zoom()} title="Reset zoom">
              <i className="far fa-dot-circle" />
            </button>
          </div>
        </div>
      </main>
    );
  }
}

export default Sigma;
