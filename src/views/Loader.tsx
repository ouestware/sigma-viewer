import React, { FC, useEffect, useState } from "react";
import { FileWithPath, useDropzone } from "react-dropzone";
import Graph from "graphology";

import LogoSigma from "../assets/logo-sigma.svg";
import LogoOuestWare from "../assets/logo-ouestware.svg";

import { loadHTTPGraphFile, loadLocalGraphFile } from "../lib/importGraph";

import "../styles/loader.css";

const FALLBACK_URL = "./les-miserables.gexf";

const Loader: FC<{ onGraphLoaded: (graph: Graph) => void }> = ({ onGraphLoaded }) => {
  // TODO:
  // Try to avoid having to read the outside world like this (without importing react-router if possible):
  const searchString = window.location.search || "";
  const searchParams = new URLSearchParams(searchString);
  const [graphPath, setGraphPath] = useState<string | null>(searchParams.get("g"));

  const [isLoading, setIsLoading] = useState<boolean>(!!graphPath);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [triedFiles, setTriedFiles] = useState<FileWithPath[]>([]);
  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    multiple: false,
  });

  // Try to load it when the user selects some file:
  useEffect(() => {
    const file: FileWithPath | null =
      acceptedFiles && acceptedFiles.length && !triedFiles.includes(acceptedFiles[0]) ? acceptedFiles[0] : null;

    if (file && !isLoading) {
      setIsLoading(true);
      loadLocalGraphFile(acceptedFiles[0])
        .then(onGraphLoaded)
        .catch((e) => setErrorMessage(e.message))
        .finally(() => {
          setTriedFiles(triedFiles.concat(file));
          setIsLoading(false);
        });
    }
  }, [acceptedFiles, isLoading, onGraphLoaded, triedFiles]);

  // Try to load the graph file when a path is specified in the URL:
  useEffect(() => {
    if (graphPath) {
      setGraphPath(null);
      setIsLoading(true);
      loadHTTPGraphFile(graphPath)
        .then(onGraphLoaded)
        .catch((e) => setErrorMessage(e.message))
        .finally(() => {
          setIsLoading(false);
        });
    }
  }, [acceptedFiles, graphPath, isLoading, onGraphLoaded]);

  return (
    <main className="loader">
      {isLoading ? (
        <div className="loading">
          <i className="fas fa-spinner fa-pulse fa-5x" />
        </div>
      ) : (
        <div className="flex">
          <div {...getRootProps({ className: "dropzone" })}>
            <input {...getInputProps()} />
            <div>
              <p className="larger">
                Drag and drop here your <strong>GEXF</strong> file...
              </p>
              <p className="large">
                ...or click here to navigate your <i className="fas fa-folder-open" /> file system...
              </p>
              <p className="large">
                ...or reload this page with a graph URL added as a search param (
                <a href="?g=./les-miserables.gexf" onClick={(e) => e.stopPropagation()}>
                  <i className="fas fa-link" /> example
                </a>
                ).
              </p>
            </div>
          </div>
          <div className="footer">
            <span>
              Made with{" "}
              <a href="http://sigmajs.org">
                <img src={LogoSigma} alt="" /> sigma.js
              </a>{" "}
              by{" "}
              <a href="https://www.ouestware.com">
                <img src={LogoOuestWare} alt="" /> OuestWare
              </a>{" "}
              (view sources on{" "}
              <a href="https://gitlab.com/ouestware/sigma-viewer">
                <i className="fab fa-gitlab" /> GitLab
              </a>
              )
            </span>
            <span>
              You can test with{" "}
              <a href={FALLBACK_URL} download={FALLBACK_URL.split("/").pop()}>
                this GEXF file
              </a>{" "}
              if you don't have any.
            </span>
          </div>
        </div>
      )}
      {errorMessage && (
        <div className="error-message" onClick={() => setErrorMessage(null)}>
          <span>{errorMessage}</span>
        </div>
      )}
    </main>
  );
};

export default Loader;
